export const toggle = {
  data() {
    return {
      isDark: false,
    }
  },
  methods: {
    theme() {
      this.isDark = !this.isDark;
    }
  }
}